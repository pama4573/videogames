import imageHorizon from '../img/imageHorizon.jpg';
import reddeadredemption from '../img/reddeadredemption.jpg';
import godofwar from '../img/godofwar.jpg';
import the_witcher3 from '../img/the_witcher3.jpg';
import hogwarts from '../img/hogwarts.jpg';
import final_fantasy from '../img/final_fantasy.jpg';
import kena from '../img/kena.jpg';
import goodbye from '../img/goodbye.jpg';
import GhostWireTokyo from '../img/GhostWireTokyo.jpg';
import Horizon from '../img/Horizon.jpg';
import bugsnax from '../img/bugsnax.jpg';
import ResidentEvilVillage from '../img/ResidentEvilVillage.jpg';
import Deathloop from '../img/Deathloop.jpg';
import Returnal from '../img/Returnal.jpg';
import JettTheFarShore from '../img/JettTheFarShore.jpg';
import GodofWarRagnarok from '../img/GodofWarRagnarok.jpg';

const videogames = [
    {name: 'Horizon Zero Dawn', price: "49.95 USD", image: imageHorizon, id: 0},
    {name: 'God Of War', price: "189,95 USD", image: godofwar, id: 1},
    {name: 'Red Dead Redemption', price: "149.95 USD", image: reddeadredemption, id: 2},
    {name: 'The Witcher 3: Wild…', price: "39.95 USD", image: the_witcher3, id: 3},
    {name: 'Hogwarts Legacy', price: "49.95 USD", image: hogwarts, id: 4},
    {name: 'Final Fantasy XVI', price: "189,95 USD", image: final_fantasy, id: 5},
    {name: 'Kena, bridge of spirits', price: "149.95 USD", image: kena, id: 6},
    {name: 'Godbye volcano highs', price: "39.95 USD", image: goodbye, id: 7},
    {name: 'GhostWire: Tokyo', price: "49.95 USD", image: GhostWireTokyo, id: 8},
    {name: 'Horizon: Forbidden West', price: "189,95 USD", image: Horizon, id: 9},
    {name: 'Bugsnax', price: "149.95 USD", image: bugsnax, id: 10},
    {name: 'Resident Evil Village', price: "39.95 USD", image: ResidentEvilVillage, id: 11},
    {name: 'Deathloop', price: "49.95 USD", image: Deathloop, id: 12},
    {name: 'Returnal', price: "189,95 USD", image: Returnal, id: 13},
    {name: 'Jett: The Far Shore', price: "149.95 USD", image: JettTheFarShore, id: 14},
    {name: 'God of War: Ragnarok', price: "39.95 USD", image: GodofWarRagnarok, id: 15}
  ];

  export default videogames