import { Route, Router , Switch } from 'react-router-dom';
import React from "react";
import DetailGame from './detailGame';
import App from '../App';
import { createBrowserHistory } from 'history';
const history = createBrowserHistory();

function AppWrapper() { 
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/:id" component={DetailGame} />
      </Switch>
    </Router>
    )
  };

  export default AppWrapper;
