import React, {Component} from 'react';
import videogames from "../assets/data/videogames";
import { useParams } from 'react-router-dom';
import '../assets/css/detailGameCss.css';
import '../assets/css/tarjetaVideogame.css';
import Boton from './buttonGame.jsx';


function DetailGame() {

    const { id } = useParams();
    const videogameId = Number(id);
    const gameSelected = videogames.find(videogame => videogame.id === videogameId);

        return  (
            <body>
                <div className="detailHearder" style={{ backgroundImage: `url(${gameSelected.image})`,backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat'}}>
                {/* <img src={gameSelected.image} alt="img-backgroung" id="detailHearder"></img> */}
                    <div className="detailHearder_container">
                        <div className="detailHearder_overline">
                            <div className="detailHearder_overlineIcon">★</div>
                            <div className="detailHearder_overlineText">New Release</div>
                        </div>
                        <h1 className="detailHearder_title">{gameSelected.name}</h1>
                    </div>
                </div>
                <div className="detailContainer">
                    <div className="detailContainer_bar">
                        <div className="detailContainer_price">{gameSelected.price}</div>
                        <div className="detailContainer_button"><Boton label="INSTALL GAME" className="rectangle button buttonInstall" onClick={() => {}} /></div>
                    </div>
                    <div className="detailContainer_desc">
                    <div className="detailContainer_photo"><img src={gameSelected.image} alt="imgDetail" width="200" height="200"></img></div>
                    <p className="detailContainer_text">Kratos, que ha dejado atrás el mundo de los dioses, deberá adaptarse a tierras que no le son familiares, afrontar peligros inesperados y aprovechar una segunda oportunidad de ejercer como padre. Junto a su hijo Atreus se aventurará en lo más profundo e inclemente de las tierras de Midgard y luchará por culminar una búsqueda hondamente personal.
• Viaja a un mundo oscuro y elemental de temibles criaturas directamente extraído de la mitología nórdica.
• Vence a tus enemigos en encarnizados combates cuerpo a cuerpo donde el pensamiento táctico y la precisión letal te darán la ventaja.
• Descubre una historia apasionante y emocional en la que Kratos se verá obligado a controlar la ira que tanto lo ha caracterizado.</p>
                    </div>
                </div> 
            </body>     
         ) 
    
}

export default DetailGame