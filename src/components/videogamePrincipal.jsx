import React, {Component} from 'react';
import '../assets/css/tarjetaVideogame.css';
import '../assets/css/videogamePrincipal.css';
import Boton from './buttonGame.jsx';

function VideogamePrincipal({name, description}){

    return (
        <div>
            <div className="videogamePrincipal">
                <div className="nameGamePrincipal">{name}</div>
                <div className="desc">{description}</div>
                <div>
                    <Boton label="INSTALL GAME" className="rectangle button butonInstall" onClick={() => {}} />
                    <Boton label="ADD TO FAVORITES" className="buttonFavorites addFavorites" onClick={() => {}} />
                </div>
            </div>
        </div>
      );
}

export default VideogamePrincipal
