import React, {  Component } from 'react';

function Boton({label, className, onGameSelected}) {

    return (
        <button className={className} onClick={(id) => onGameSelected(id)}>{label}</button>
    )
}
export default Boton