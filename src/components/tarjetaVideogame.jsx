import React, {Component} from 'react';
import '../assets/css/tarjetaVideogame.css';
import Boton from './buttonGame.jsx';

function Tarjeta({name, price, image, id, onGameSelected}) {

    return (
        <div>
            <div><img src={image} alt="img1" className="image"></img></div>
            <div className="videogame-info">
                <div className="name"> {name} <p className="-USD"> {price}</p></div>
                <div><Boton label="BUY NOW" className="rectangle button" onGameSelected={ () => onGameSelected(id) } /></div>
            </div>
        </div>
      );
}

export default Tarjeta