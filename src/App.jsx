import { useState } from 'react';
import Tarjeta from './components/tarjetaVideogame.jsx';
import VideogamePrincipal from './components/videogamePrincipal.jsx';
import React from 'react';
import leagueoflegendsbackground from './assets/img/leagueoflegendsbackground.jpg';
import videogames from './assets/data/videogames';
import { useHistory, browserHistory } from 'react-router-dom';

function App() {
  const history = useHistory();
  const [gamePrincipal, setGamePrincipal] = useState(
    { name: 'League of Legends', 
    description: "League of Legends is a fast-paced, competitive online game that blends the speed and intensity of an RTS with RPG elements. "}
  );

  const handleNavigation = (id) => {
    history.push(`/${id}`)
    /* history.push("/"+id) */
  }

  return (
    <body className="Simplified-homepage">
      <div className="divPrincipal">
        <img src={leagueoflegendsbackground} class="League-of-legends_Background" alt="img-backgroung"></img>
        <p className="awarded name">AWARDED AND RECOMMENDED</p>
        <VideogamePrincipal name={gamePrincipal.name} description={gamePrincipal.description} />
      </div>
      <div className="list-videogames">
        <p className="tittle">BESTSELLERS</p>
        <div className="displayVideogames">
          {videogames.map(videogame => (
            <Tarjeta name={videogame.name} price={videogame.price} image={videogame.image} id={videogame.id} onGameSelected={handleNavigation} />
          ))}
        </div>
      </div>
    </body>

  );
}

export default App;
